﻿namespace SecondsConversion
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.promptLabel = new System.Windows.Forms.Label();
            this.secondsTextBox = new System.Windows.Forms.TextBox();
            this.conversionButton = new System.Windows.Forms.Button();
            this.endingLabel = new System.Windows.Forms.Label();
            this.convertedValueLabel = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // promptLabel
            // 
            this.promptLabel.Location = new System.Drawing.Point(26, 85);
            this.promptLabel.Name = "promptLabel";
            this.promptLabel.Size = new System.Drawing.Size(148, 23);
            this.promptLabel.TabIndex = 0;
            this.promptLabel.Text = "Enter a number of seconds:";
            this.promptLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // secondsTextBox
            // 
            this.secondsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.secondsTextBox.Location = new System.Drawing.Point(254, 88);
            this.secondsTextBox.Name = "secondsTextBox";
            this.secondsTextBox.Size = new System.Drawing.Size(100, 20);
            this.secondsTextBox.TabIndex = 1;
            // 
            // conversionButton
            // 
            this.conversionButton.Location = new System.Drawing.Point(176, 160);
            this.conversionButton.Name = "conversionButton";
            this.conversionButton.Size = new System.Drawing.Size(84, 49);
            this.conversionButton.TabIndex = 2;
            this.conversionButton.Text = "Convert the Value Entered";
            this.conversionButton.UseVisualStyleBackColor = true;
            this.conversionButton.Click += new System.EventHandler(this.conversionButton_Click);
            // 
            // endingLabel
            // 
            this.endingLabel.Location = new System.Drawing.Point(26, 254);
            this.endingLabel.Name = "endingLabel";
            this.endingLabel.Size = new System.Drawing.Size(148, 35);
            this.endingLabel.TabIndex = 3;
            this.endingLabel.Text = "The amount of seconds entered is equal to:";
            this.endingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // convertedValueLabel
            // 
            this.convertedValueLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.convertedValueLabel.Location = new System.Drawing.Point(254, 266);
            this.convertedValueLabel.Name = "convertedValueLabel";
            this.convertedValueLabel.Size = new System.Drawing.Size(100, 23);
            this.convertedValueLabel.TabIndex = 4;
            this.convertedValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(176, 347);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(84, 23);
            this.clearButton.TabIndex = 5;
            this.clearButton.Text = "Clear All";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 417);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.convertedValueLabel);
            this.Controls.Add(this.endingLabel);
            this.Controls.Add(this.conversionButton);
            this.Controls.Add(this.secondsTextBox);
            this.Controls.Add(this.promptLabel);
            this.Name = "Form1";
            this.Text = "Seconds Conversion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label promptLabel;
        private System.Windows.Forms.TextBox secondsTextBox;
        private System.Windows.Forms.Button conversionButton;
        private System.Windows.Forms.Label endingLabel;
        private System.Windows.Forms.Label convertedValueLabel;
        private System.Windows.Forms.Button clearButton;
    }
}

