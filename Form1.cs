﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecondsConversion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void conversionButton_Click(object sender, EventArgs e)
        {
            double time;

            double output;

           //Converts string entered into textbox to double if possible.
            if (double.TryParse(secondsTextBox.Text, out time))
            {
                //Converts seconds into days, hours, or minutes and displays the final value rounded to 3 decimal places.
                if (time >= 86400) {

                    output = time / 86400;
                    output = Math.Round(output, 3);
                    convertedValueLabel.Text = output.ToString() + " days";

                } else if (time >= 3600) {

                    output = time / 3600;
                    output = Math.Round(output, 3);
                    convertedValueLabel.Text = output.ToString() + " hours";

                } else if (time >= 60) {

                    output = time / 60;
                    output = Math.Round(output, 3);
                    convertedValueLabel.Text = output.ToString() + " minutes";

                } else {

                    output = time;
                    output = Math.Round(output, 3);
                    convertedValueLabel.Text = output.ToString() + " seconds";
                }
            }
            else {
                //Prompts the user to change their input.
                MessageBox.Show("Please enter a number");
            }

        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            //Clears the secondsTextBox.
            secondsTextBox.Text = "";

            //Clears the convertedValueLabel.
            convertedValueLabel.Text = "";

        }
    }
}
